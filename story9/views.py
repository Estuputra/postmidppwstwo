from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import RegisterForm, LoginForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User


# Create your views here.


def register(request):
    print(request.user.is_authenticated)
    if request.user.is_authenticated:
        # redirect page home
        return redirect('/')
    else:
        form = RegisterForm()
        if request.method == "POST":
            form_input = RegisterForm(request.POST)
            if form_input.is_valid():
                username_input = request.POST['username']
                password_input = request.POST['password1']
                new_user = User.objects.create_user(
                    username=username_input, email=request.POST['email'], password=password_input)
                new_user.save()
                user_login = authenticate(
                    request, username=username_input, password=password_input)
                login(request, user_login)
                return redirect('/')
            else:
                return render(request, 'story9/register.html', {'form': form_input})
        return render(request, 'story9/register.html', {'form': form})


def loginCustom(request):
    if request.user.is_authenticated:
        return redirect('/')
    form = LoginForm()
    if request.method == "POST":
        data = LoginForm(request.POST)
        if data.is_valid():
            username_input = request.POST['username']
            password_input = request.POST['password']
            user_login = authenticate(
                request, username=username_input, password=password_input)
            print(user_login)
            if user_login is None:
                return render(request, 'story9/login.html', {'status': 'failed', 'form': form})
            else:
                login(request, user_login)
                return redirect('/')
        else:
            return render(request, 'story9/login.html', {'status': 'failed', 'form': data})
    else:
        return render(request, 'story9/login.html', {'form': form})


def index(request):
    return render(request, 'story9/index.html')


@login_required
def logoutCustom(request):
    logout(request)
    return redirect('/login')
#

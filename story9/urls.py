from django.urls import path

from . import views
urlpatterns = [
    path('', views.index, name='story9index'),
    path('register/', views.register, name="register"),
    path('login/', views.loginCustom, name='login'),
    path('logout/', views.logoutCustom, name='logout'),
]

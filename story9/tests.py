from .apps import Story9Config
from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps
from django.contrib.auth.models import User
from .forms import LoginForm, RegisterForm


# Create your tests here.
class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(Story9Config.name, 'story9')
        self.assertEqual(apps.get_app_config('story9').name, 'story9')


class Story9UnitTest(TestCase):
    def test_homepage_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_signup_url_exists(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_logout_url_exist(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

    def test_login_url_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_forms_login_valid(self):
        form_login = LoginForm(data={
            "username": "hahaha",
            "password": "hihihihi123"
        })
        self.assertTrue(form_login.is_valid())

    def test_forms_login_invalid(self):
        form_login = LoginForm(data={
            "username": "a",
            "password": ""
        })
        self.assertFalse(form_login.is_valid())

    def test_forms_register_valid(self):
        form_reg = RegisterForm(data={
            "username": "hahaha",
            "email": "haha@gmail.com",
            'password1': '123321hahaha',
            'password2': '123321hahaha',
        })
        self.assertTrue(form_reg.is_valid())

    def test_forms_register_valid(self):
        form1 = RegisterForm(data={
            "username": "",
            "email": "haha@gmail.com",
            'password1': '123321hahaha',
            'password2': '123321a',
        })
        self.assertFalse(form1.is_valid())


class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.login = reverse("login")
        self.reg = reverse("register")

    def test_POST_login_valid(self):
        response = self.client.post(self.login,
                                    {
                                        'username': 'belajar',
                                        'password ': "belajar1PPW"
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_login_invalid(self):
        response = self.client.post(self.login,
                                    {
                                        'username': '',
                                        'password ': ""
                                    }, follow=True)
        self.assertTemplateUsed(response, 'story9/login.html')

    def test_POST_reg_valid(self):
        response = self.client.post(self.reg,
                                    {
                                        "username": "hahaha",
                                        "email": "haha@gmail.com",
                                        'password1': '123321hahaha',
                                        'password2': '123321hahaha',
                                    }, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_POST_reg_invalid(self):
        response = self.client.post(self.reg,
                                    {
                                        "username": "hahaha",
                                        "email": "hagmail.com",
                                        'password1': '123321hahaha',
                                        'password2': '',
                                    }, follow=True)
        self.assertTemplateUsed(response, 'story9/register.html')

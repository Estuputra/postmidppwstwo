from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.apps import apps

# from selenium import webdriver
# from selenium.webdriver.chrome.options import Options

from .apps import Story7Config
from .views import index
import time


class UnitTest(TestCase):
    def test_get_complete(self):
        response = Client().get('')
        self.assertTrue(response.status_code, 200)

    def test_url(self):
        found = resolve(reverse('index7'))
        self.assertTrue(found.func, index
                        )


# class FunctionalTest(LiveServerTestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')

#         self.browser = webdriver.Chrome(
#             './chromedriver', chrome_options=chrome_options)
#         super(FunctionalTest, self).setUp()

#     def tearDown(self):
#         self.browser.quit()
#         return super(FunctionalTest, self).tearDown()

#     def test(self):
#         self.assertTrue(2, 32)

#     def testLagi(self):
#         self.browser.get(self.live_server_url + '/')

#         # Cek judul page
#         self.assertIn('Know me more!', self.browser.page_source)

#         # Tulisan content belum diperlihatkan dikarenakan accordion belum ada yang diklik
#         content = self.browser.find_element_by_tag_name('body').text
#         self.assertNotIn('Belajar', content)

#         # Klik bar pertama yaitu (Aktivitas sekarang)
#         bar = self.browser.find_element_by_id('activity')
#         self.assertIn('Aktivitas Sekarang', bar.text)
#         bar.click()
#         time.sleep(1)

#         # Content terlihat
#         text = self.browser.find_element_by_tag_name('body').text
#         self.assertIn('Belajar', text)

#         # Text content kedua masih belum terlihat
#         self.assertNotIn('experience', text)

#         # Klik accordion kedua
#         bar = self.browser.find_element_by_id('organization')
#         self.assertIn('Pengalaman Organisasi', bar.text)
#         bar.click()
#         time.sleep(1)

#         # Content terlihat
#         text = self.browser.find_element_by_tag_name('body').text
#         self.assertIn('experience', text)

#         # Text content ketiga masih belum terlihat
#         self.assertNotIn('Fasilkom', text)

#         # Klik accordion ketiga
#         bar = self.browser.find_element_by_id('prestasi')
#         self.assertIn('Prestasi', bar.text)
#         bar.click()
#         time.sleep(1)

#         # Content terlihat
#         text = self.browser.find_element_by_tag_name('body').text
#         self.assertIn('Fasilkom', text)

#         # Text content ketiga masih belum terlihat
#         self.assertNotIn('Lariyuk', text)

#         # Klik accordion ketiga
#         bar = self.browser.find_element_by_id('hobby')
#         self.assertIn('Hobby', bar.text)
#         bar.click()
#         time.sleep(1)

#         # Content terlihat
#         text = self.browser.find_element_by_tag_name('body').text
#         self.assertIn('Lariyuk', text)

#         # Conten lain tidak terlihat
#         self.assertNotIn('experience', text)

#         # Test naik turun untuk segmen hobby
#         up = self.browser.find_element_by_css_selector('#hobby + div .up')
#         up.click()
#         up.click()
#         up.click()
#         time.sleep(1)

#         # Hobby berada di atas
#         text = self.browser.find_element_by_tag_name('body').text
#         self.assertTrue(text.find('hobby') < text.find('Prestasi'))
#         self.assertTrue(text.find('hobby') <
#                         text.find('Pengalaman Organisasi'))
#         self.assertTrue(text.find('hobby') < text.find('Aktivitas Sekarang'))

#         # Turunin lagi
#         down = self.browser.find_element_by_css_selector('#hobby + div .down')
#         down.click()
#         down.click()
#         down.click()
#         time.sleep(1)

#         # Hobby turun
#         text = self.browser.find_element_by_tag_name('body').text
#         self.assertTrue(text.find('Aktivitas Sekarang')
#                         > text.find('hobby'))


class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(Story7Config.name, 'story7')
        self.assertEqual(apps.get_app_config('story7').name, 'story7')

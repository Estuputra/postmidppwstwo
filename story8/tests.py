from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.apps import apps

from . import views
from .apps import Story8Config


class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(Story8Config.name, 'story8')
        self.assertEqual(apps.get_app_config('story8').name, 'story8')


class UnitTest(TestCase):
    def test_views_index_exists(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 302)

    def test_index_url(self):
        found = resolve(reverse('index8'))
        self.assertTrue(found.func, views.index)

    def test_index_url(self):
        found = resolve(reverse('ajax', args=[1, 'lol']))
        self.assertTrue(found.func, views.cobaAjax)

    def test_views_ajax(self):
        response = Client().get('/story8/ajax/1/lol')
        self.assertEqual(response.status_code, 301)
        #

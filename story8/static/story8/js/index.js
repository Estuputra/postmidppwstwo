$(document).ready(() => {
    if (window.location.href.match(/story8/)) {
        search("tottenham",1);
        $("#searchBuku").keyup((e) =>{
            bookTitle = e.currentTarget.value.toLowerCase();
            console.log(bookTitle);
            search(bookTitle, 1);
        });
        
    }

    
    

    function search(booksName, halaman) {
        
            $('.numberNavigator').html("" + halaman);
           
            // console.log(halaman+"halamannih")
            if(booksName==""){
                booksName="tottenham"
            }
            $.ajax({
                url: "/story8/ajax/" + halaman+"/"+booksName,
                success: function (response) {
                    // console.log(response.items[0].volumeInfo)
                    let banyakBuku = response.totalItems;
                    if(banyakBuku == 0 ){
                        alert('Tidak ada buku bernama ' +booksName);
                    } else if(banyakBuku / 10 < halaman - 1 ){
                        alert("Sudah batas maximum");
                        window.location = '/story8/';
                    }
                    $('tbody').empty();
                    let x = "";
                    for (let i = 0 ; i < response.items.length; i++) {
                   
                        try {
                            let pengali = (halaman*10) - 10
                            x += "<tr>" + "<td>" +  (i + 1 + pengali)  +"</td>"+  "<td>" +  response.items[i].volumeInfo.title +"</td>"
                            + "<td>" +  response.items[i].volumeInfo.authors[0] +"</td>"+
                            "<td>" +"<img src = '"+response.items[i].volumeInfo.imageLinks.thumbnail+"'> </td>"
                            + "</tr>";
                           
                        } catch(error)  {
                           
                        }
                      
                    }
                    $('tbody').append(x);
                }
            });
        
    }

    function prev(booksName, halamanSebelum) {
        if(halamanSebelum == 1) {
            alert("Sudah pada halaman pertama :)");
            window.location = '/story8/';
        } else{
            console.log(halamanSebelum-1)
            search(booksName, halamanSebelum - 1);
        }
    }

    $('.prev').on("click",() =>{
        let booksName = $('#searchBuku').val();
        let halamanNow = parseInt($('.numberNavigator').text());
        // console.log(halamanNow)
        prev(booksName, halamanNow);
    });

    function next(booksName, halamanSebelum) {
            search(booksName, halamanSebelum + 1);
        
    }

    $('.next').on('click',() =>{
        let booksName = $('#searchBuku').val();
        if(booksName == ""){
            booksName = "tottenham";
        }
        let halamanNow = parseInt($('.numberNavigator').text());

        next(booksName, halamanNow);
    });
});
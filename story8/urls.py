from django.urls import path

from . import views
urlpatterns = [
    path('demo/', views.demo),
    path('', views.index, name='index8'),  # url
    path('ajax/<int:start_index>/<str:book_name>/', views.cobaAjax, name='ajax')
]
